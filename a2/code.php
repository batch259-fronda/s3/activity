<?php

class Building {

	public $name;
	public readonly string $floor;
	public readonly string $address;

	public function __construct($name, $floor, $address) {

		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getFloor() {
		return $this->floor;
	}

	public function getAddress() {
		return $this->address;
	}

};

class Condominium extends Building {

};

$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');